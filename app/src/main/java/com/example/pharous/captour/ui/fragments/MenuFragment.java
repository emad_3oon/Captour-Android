package com.example.pharous.captour.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.pharous.captour.R;
import com.example.pharous.captour.base.BaseFragment;
import com.example.pharous.captour.ui.adapters.MenuAdapter;
import com.example.pharous.captour.ui.models.MenuModel;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 */
public class MenuFragment extends BaseFragment {

    Unbinder unbinder;

    @BindView(R.id.menu_recyclerView)
    RecyclerView menu_recyclerView;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu, container, false);

        unbinder = ButterKnife.bind(this, view);
        initRecyclerView();
        return view;
    }

    private void initRecyclerView(){
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getContext(), 2);
        menu_recyclerView.setLayoutManager(layoutManager);
        menu_recyclerView.setAdapter(initAdapter());
    }

    private MenuAdapter initAdapter() {
        return new MenuAdapter(getContext(), initMenuModelsList());
    }

    private ArrayList<MenuModel> initMenuModelsList(){
        ArrayList<MenuModel> menuModels = new ArrayList<>();
        menuModels.add(new MenuModel(getString(R.string.invite_friends),
                Objects.requireNonNull(getContext()).getResources().getDrawable(R.drawable.ic_invitefriends)));
        menuModels.add(new MenuModel(getString(R.string.recent_search),
                Objects.requireNonNull(getContext()).getResources().getDrawable(R.drawable.ic_search)));
        menuModels.add(new MenuModel(getString(R.string.remove_ads),
                Objects.requireNonNull(getContext()).getResources().getDrawable(R.drawable.ic_clear)));
        menuModels.add(new MenuModel(getString(R.string.change_language),
                Objects.requireNonNull(getContext()).getResources().getDrawable(R.drawable.ic_language)));
        menuModels.add(new MenuModel(getString(R.string.help_and_support),
                Objects.requireNonNull(getContext()).getResources().getDrawable(R.drawable.ic_help)));
        menuModels.add(new MenuModel(getString(R.string.sign_out),
                Objects.requireNonNull(getContext()).getResources().getDrawable(R.drawable.ic_signout)));

        return menuModels;
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}
