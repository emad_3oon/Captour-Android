package com.example.pharous.captour.ui.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import com.example.pharous.captour.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LanguageListAdapter extends RecyclerView.Adapter<LanguageListAdapter.ViewHolder>{

    private ArrayList<String> list;

    public LanguageListAdapter(ArrayList<String> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public LanguageListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_language_view, parent, false);
        return new LanguageListAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull LanguageListAdapter.ViewHolder holder, int position) {
        changeView(holder, position);
        fillData(holder, position);
    }

    private void changeView(LanguageListAdapter.ViewHolder holder, int position){
        if(position == list.size()-1){
            holder.languageItem_line.setVisibility(View.GONE);
        }
    }

    private void fillData(LanguageListAdapter.ViewHolder holder, int position) {
        holder.languageItem_textView.setText(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.languageItem_textView)
        TextView languageItem_textView;
        @BindView(R.id.languageItem_line)
        View languageItem_line;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

        }

    }

}
