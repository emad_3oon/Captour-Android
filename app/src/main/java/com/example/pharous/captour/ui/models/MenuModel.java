package com.example.pharous.captour.ui.models;

import android.graphics.drawable.Drawable;

public class MenuModel {

    private String title;
    private Drawable image;

    public MenuModel(String title, Drawable image) {
        this.title = title;
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Drawable getImage() {
        return image;
    }

    public void setImage(Drawable image) {
        this.image = image;
    }
}
