package com.example.pharous.captour.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.pharous.captour.R;
import com.example.pharous.captour.base.BaseFragment;
import com.example.pharous.captour.ui.adapters.RecentSearchesAdapter;
import com.example.pharous.captour.ui.models.RecentSearchesModel;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 */
public class RecentSearchesFragment extends BaseFragment {

    Unbinder unbinder;

    @BindView(R.id.recentSearches_recyclerView)
    RecyclerView recentSearches_recyclerView;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recent_searches, container, false);

        unbinder = ButterKnife.bind(this, view);
        initRecyclerView();
        return view;
    }

    private void initRecyclerView(){
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recentSearches_recyclerView.setLayoutManager(layoutManager);
        recentSearches_recyclerView.setAdapter(initAdapter());
    }

    private RecentSearchesAdapter initAdapter() {
        return new RecentSearchesAdapter(getContext(), initRecentSearchesModelsList());
    }

    private ArrayList<RecentSearchesModel> initRecentSearchesModelsList(){
        ArrayList<RecentSearchesModel> recentSearchesModels = new ArrayList<>();
        recentSearchesModels.add(new RecentSearchesModel("Castle", "18:32 one day ago",
                "address", "long time ago", "town", "more details",
                Objects.requireNonNull(getContext()).getResources().getDrawable(R.drawable.test4)));
        recentSearchesModels.add(new RecentSearchesModel("Castle", "18:32 one day ago",
                "address", "long time ago", "town", "more details",
                Objects.requireNonNull(getContext()).getResources().getDrawable(R.drawable.test2)));
        recentSearchesModels.add(new RecentSearchesModel("Castle", "18:32 one day ago",
                "address", "long time ago", "town", "more details",
                Objects.requireNonNull(getContext()).getResources().getDrawable(R.drawable.test6)));
        recentSearchesModels.add(new RecentSearchesModel("Castle", "18:32 one day ago",
                "address", "long time ago", "town", "more details",
                Objects.requireNonNull(getContext()).getResources().getDrawable(R.drawable.test2)));
        recentSearchesModels.add(new RecentSearchesModel("Castle", "18:32 one day ago",
                "address", "long time ago", "town", "more details",
                Objects.requireNonNull(getContext()).getResources().getDrawable(R.drawable.test4)));
        recentSearchesModels.add(new RecentSearchesModel("Castle", "18:32 one day ago",
                "address", "long time ago", "town", "more details",
                Objects.requireNonNull(getContext()).getResources().getDrawable(R.drawable.test5)));
        recentSearchesModels.add(new RecentSearchesModel("Castle", "18:32 one day ago",
                "address", "long time ago", "town", "more details",
                Objects.requireNonNull(getContext()).getResources().getDrawable(R.drawable.test3)));
        recentSearchesModels.add(new RecentSearchesModel("Castle", "18:32 one day ago",
                "address", "long time ago", "town", "more details",
                Objects.requireNonNull(getContext()).getResources().getDrawable(R.drawable.test1)));
        return recentSearchesModels;
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}
