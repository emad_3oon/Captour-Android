package com.example.pharous.captour.ui.models;

import android.graphics.drawable.Drawable;

import java.io.Serializable;

public class RecentSearchesModel implements Serializable {

    private String placeName;
    private String searchTime;
    private String placeDirection;
    private String built;
    private String owner;
    private String moreDetails;
    private Drawable image;

    public RecentSearchesModel(String placeName, String searchTime, String placeDirection,
                               String built, String owner, String moreDetails, Drawable image) {
        this.placeName = placeName;
        this.searchTime = searchTime;
        this.placeDirection = placeDirection;
        this.built = built;
        this.owner = owner;
        this.moreDetails = moreDetails;
        this.image = image;
    }

    public Drawable getImage() {
        return image;
    }

    public void setImage(Drawable image) {
        this.image = image;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public String getSearchTime() {
        return searchTime;
    }

    public void setSearchTime(String searchTime) {
        this.searchTime = searchTime;
    }

    public String getPlaceDirection() {
        return placeDirection;
    }

    public void setPlaceDirection(String placeDirection) {
        this.placeDirection = placeDirection;
    }

    public String getBuilt() {
        return built;
    }

    public void setBuilt(String built) {
        this.built = built;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getMoreDetails() {
        return moreDetails;
    }

    public void setMoreDetails(String moreDetails) {
        this.moreDetails = moreDetails;
    }
}
