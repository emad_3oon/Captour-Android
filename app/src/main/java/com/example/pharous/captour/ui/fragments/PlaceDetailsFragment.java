package com.example.pharous.captour.ui.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.pharous.captour.R;
import com.example.pharous.captour.base.BaseFragment;
import com.example.pharous.captour.ui.models.RecentSearchesModel;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 */
public class PlaceDetailsFragment extends BaseFragment {

    Unbinder unbinder;

    @BindView(R.id.placeBuilt_textView)
    TextView placeBuiltTextView;
    @BindView(R.id.placeOwner_textView)
    TextView placeOwnerTextView;
    @BindView(R.id.moreDetails_textView)
    TextView moreDetailsTextView;
    @BindView(R.id.placeImageView)
    ImageView placeImageView;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_place_details, container, false);

        unbinder = ButterKnife.bind(this, view);
        initView();
        return view;
    }

    private void initView(){
        RecentSearchesModel recentSearchesModel =
                (RecentSearchesModel) Objects.requireNonNull(getArguments()).getSerializable("place-details");
        placeBuiltTextView.setText(Objects.requireNonNull(recentSearchesModel).getBuilt());
        placeOwnerTextView.setText(Objects.requireNonNull(recentSearchesModel).getOwner());
        moreDetailsTextView.setText(Objects.requireNonNull(recentSearchesModel).getMoreDetails());
        placeImageView.setImageDrawable(recentSearchesModel.getImage());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
