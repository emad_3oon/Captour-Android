package com.example.pharous.captour.ui.adapters;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.pharous.captour.R;
import com.example.pharous.captour.ui.fragments.PlaceDetailsFragment;
import com.example.pharous.captour.ui.models.RecentSearchesModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.pharous.captour.tools.Preferences.Methods.editToolbar;
import static com.example.pharous.captour.tools.Preferences.Methods.openFragment;
import static com.example.pharous.captour.tools.Preferences.colors.CYAN_BLUE;
import static com.example.pharous.captour.tools.Preferences.colors.WHITE;

public class RecentSearchesAdapter extends RecyclerView.Adapter<RecentSearchesAdapter.ViewHolder>{

    private Activity context;
    private ArrayList<RecentSearchesModel> recentSearchesModels;

    public RecentSearchesAdapter(Activity context, ArrayList<RecentSearchesModel> recentSearchesModels) {
        this.context = context;
        this.recentSearchesModels = recentSearchesModels;
    }

    @NonNull
    @Override
    public RecentSearchesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recent_searches_view,
                parent, false);
        return new RecentSearchesAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecentSearchesAdapter.ViewHolder holder, int position) {
        fillData(holder, position);
    }

    private void fillData(RecentSearchesAdapter.ViewHolder holder, int position) {
        holder.placeTitle_textView.setText(recentSearchesModels.get(position).getPlaceName());
        holder.searchTime_textView.setText(recentSearchesModels.get(position).getSearchTime());
        holder.placeDirection_textView.setText(recentSearchesModels.get(position).getPlaceDirection());
        holder.placeImageView.setImageDrawable(recentSearchesModels.get(position).getImage());
    }

    @Override
    public int getItemCount() {
        return recentSearchesModels.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.placeTitle_textView) TextView placeTitle_textView;
        @BindView(R.id.searchTime_textView) TextView searchTime_textView;
        @BindView(R.id.placeDirection_textView) TextView placeDirection_textView;
        @BindView(R.id.placeView_cardView)
        CardView placeView_cardView;
        @BindView(R.id.placeImageView)
        ImageView placeImageView;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            placeView_cardView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Bundle bundle = new Bundle();
            bundle.putSerializable("place-details", recentSearchesModels.get(getAdapterPosition()));
            Fragment fragment = openFragment(context, PlaceDetailsFragment.class, R.id.homeContainer_frame);
            fragment.setArguments(bundle);
            editToolbar(context, recentSearchesModels.get(getAdapterPosition()).getPlaceName(),
                    CYAN_BLUE, WHITE, R.drawable.ic_back_blue);
        }

    }

}
