package com.example.pharous.captour.google.ads;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import static com.example.pharous.captour.tools.Preferences.Methods.isNetworkAvailable;

public class GoogleAdsTools {

    private final static String ADS_APP_ID = "ca-app-pub-3458113504043412~5522064852";

    public static void showAds(final Activity context, AdView adView, final FrameLayout adsFrame){
        if(isNetworkAvailable(context)){
            initAds(context, adView, adsFrame);
        }else {
            Toast.makeText(context, "NO NETWORK AVAILABLE", Toast.LENGTH_LONG).show();
        }
    }

    private static void initAds(final Activity context, AdView adView, final FrameLayout adsFrame){

        MobileAds.initialize(context, GoogleAdsTools.ADS_APP_ID);
        adsFrame.setVisibility(View.VISIBLE);

        adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                Toast.makeText(context, "AD LOAD", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                Toast.makeText(context, "AD LOAD FAILED", Toast.LENGTH_SHORT).show();
                adsFrame.setVisibility(View.GONE);
                Log.e( "AD LOAD FAILED", errorCode+"");
            }

            @Override
            public void onAdOpened() {
                Toast.makeText(context, "AD OPED", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLeftApplication() {
                Toast.makeText(context, "APP LEFT", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdClosed() {
                Toast.makeText(context, "AD CLOSED", Toast.LENGTH_SHORT).show();
            }
        });

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
    }

}
