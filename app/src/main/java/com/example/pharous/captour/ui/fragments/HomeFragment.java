package com.example.pharous.captour.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.example.pharous.captour.R;
import com.example.pharous.captour.base.BaseFragment;
import com.example.pharous.captour.ui.adapters.HomeListAdapter;
import com.example.pharous.captour.ui.models.HomeListModel;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.example.pharous.captour.google.ads.GoogleAdsTools.showAds;
import static com.example.pharous.captour.tools.Preferences.Methods.openFragment;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends BaseFragment implements HomeListAdapter.ListItemClickListener{

    Unbinder unbinder;

    @BindView(R.id.home_recyclerView)
    RecyclerView home_recyclerView;
    @BindView(R.id.adView)
    AdView adView;
    @BindView(R.id.ads_frame)
    FrameLayout ads_frame;

    private LinearLayoutManager layoutManager;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        unbinder = ButterKnife.bind(this, view);
        initRecyclerView();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        showAds(getContext(), adView, ads_frame);
    }

    private void initRecyclerView(){
        layoutManager = new LinearLayoutManager(getContext(),
                LinearLayoutManager.HORIZONTAL, false);
        home_recyclerView.setLayoutManager(layoutManager);
        home_recyclerView.setAdapter(initAdapter());
    }

    private HomeListAdapter initAdapter() {
        return new HomeListAdapter(initHomeModelsList(), this);
    }

    private ArrayList<HomeListModel> initHomeModelsList(){
        ArrayList<HomeListModel> homeListModels = new ArrayList<>();
        homeListModels.add(new HomeListModel(getString(R.string.invite_friends),
                Objects.requireNonNull(getContext()).getResources().getDrawable(R.drawable.test1)));
        homeListModels.add(new HomeListModel(getString(R.string.recent_search),
                Objects.requireNonNull(getContext()).getResources().getDrawable(R.drawable.test2)));
        homeListModels.add(new HomeListModel(getString(R.string.remove_ads),
                Objects.requireNonNull(getContext()).getResources().getDrawable(R.drawable.test3)));
        homeListModels.add(new HomeListModel(getString(R.string.change_language),
                Objects.requireNonNull(getContext()).getResources().getDrawable(R.drawable.test4)));
        homeListModels.add(new HomeListModel(getString(R.string.help_and_support),
                Objects.requireNonNull(getContext()).getResources().getDrawable(R.drawable.test5)));
        homeListModels.add(new HomeListModel(getString(R.string.sign_out),
                Objects.requireNonNull(getContext()).getResources().getDrawable(R.drawable.test6)));

        return homeListModels;
    }

    @OnClick({R.id.rowLeft_imageView, R.id.rowRight_imageView, R.id.discoverMap_button,
            R.id.camera_frame, R.id.takeTour_cardView})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rowLeft_imageView:
                home_recyclerView.scrollToPosition(layoutManager.findFirstVisibleItemPosition()-1);
                break;
            case R.id.rowRight_imageView:
                home_recyclerView.scrollToPosition(layoutManager.findLastVisibleItemPosition()+1);
                break;
            case R.id.discoverMap_button:
                break;
            case R.id.camera_frame:
                break;
            case R.id.takeTour_cardView:
                openFragment(getContext(), TakeTourFragment.class, R.id.homeContainer_frame);
                break;
        }
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onListItemClick() {

    }
}
