package com.example.pharous.captour.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import com.example.pharous.captour.R;
import com.example.pharous.captour.base.BaseFragment;
import com.example.pharous.captour.ui.activities.HomeActivity;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.example.pharous.captour.tools.Preferences.Methods.openFragment;


/**
 * A simple {@link Fragment} subclass.
 */
public class TakeTourFragment extends BaseFragment implements SeekBar.OnSeekBarChangeListener {

    Unbinder unbinder;

    @BindView(R.id.takeTour_imageView)
    ImageView takeTourImageView;
    @BindView(R.id.takeTourTitle_textView)
    TextView takeTourTitleTextView;
    @BindView(R.id.next_seekBar)
    SeekBar nextSeekBar;
    @BindView(R.id.skipTutorial_textView)
    TextView skipTutorialTextView;
    @BindView(R.id.tourDone_cardView)
    CardView tourDoneCardView;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_take_tour, container, false);

        unbinder = ButterKnife.bind(this, view);
        initView();
        return view;
    }

    private void initView() {
        nextSeekBar.setOnSeekBarChangeListener(this);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(((HomeActivity) Objects.requireNonNull(getContext())).getSupportActionBar()).hide();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void controlSeekBar(int progress) {
        if (progress >= 25 && progress < 50) {
            takeTourImageView.setImageDrawable(Objects.requireNonNull(getContext()).
                    getResources().getDrawable(R.drawable.ic_capture));
            takeTourTitleTextView.setText(getContext().getResources().getString(R.string.capture_anything));
        }
        if (progress >= 50 && progress < 75) {
            takeTourImageView.setImageDrawable(Objects.requireNonNull(getContext()).
                    getResources().getDrawable(R.drawable.ic_ai));
            takeTourTitleTextView.setText(getContext().getResources().getString(R.string.the_artificial_intelligence_work));
        }
        if (progress >= 75 && progress <= 100) {
            takeTourImageView.setImageDrawable(Objects.requireNonNull(getContext()).
                    getResources().getDrawable(R.drawable.ic_mobile_share));
            takeTourTitleTextView.setText(getContext().getResources().getString(R.string.share_results));
        }
        if (progress < 25) {
            takeTourImageView.setImageDrawable(Objects.requireNonNull(getContext()).
                    getResources().getDrawable(R.drawable.ic_travel));
            takeTourTitleTextView.setText(getContext().getResources().getString(R.string.travel_anywhere));
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        controlSeekBar(progress);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        controlSeekBar(seekBar.getProgress());
        if (seekBar.getProgress() > 75) {
            skipTutorialTextView.setVisibility(View.GONE);
            seekBar.setVisibility(View.INVISIBLE);
            tourDoneCardView.setVisibility(View.VISIBLE);
        }
    }

    @OnClick({R.id.skipTutorial_textView, R.id.tourDone_cardView})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.skipTutorial_textView:
                exitTutorial();
                break;
            case R.id.tourDone_cardView:
                exitTutorial();
                break;
        }
    }

    private void exitTutorial(){
        openFragment(getContext(), HomeFragment.class, R.id.homeContainer_frame);
        Objects.requireNonNull(((HomeActivity) Objects.requireNonNull(getContext())).getSupportActionBar()).show();
    }
}
