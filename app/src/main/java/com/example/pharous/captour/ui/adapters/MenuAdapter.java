package com.example.pharous.captour.ui.adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.example.pharous.captour.R;
import com.example.pharous.captour.ui.fragments.ChangeLanguageFragment;
import com.example.pharous.captour.ui.fragments.HelpFragment;
import com.example.pharous.captour.ui.fragments.HomeFragment;
import com.example.pharous.captour.ui.fragments.InviteFriendsFragment;
import com.example.pharous.captour.ui.fragments.RecentSearchesFragment;
import com.example.pharous.captour.ui.fragments.RemoveAdsFragment;
import com.example.pharous.captour.ui.models.MenuModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.pharous.captour.tools.Preferences.Methods.editToolbar;
import static com.example.pharous.captour.tools.Preferences.Methods.openFragment;
import static com.example.pharous.captour.tools.Preferences.colors.CYAN_BLUE;
import static com.example.pharous.captour.tools.Preferences.colors.WHITE;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.ViewHolder>{

    private Activity context;
    private ArrayList<MenuModel> menuModels;

    public MenuAdapter(Activity context, ArrayList<MenuModel> menuModels) {
        this.context = context;
        this.menuModels = menuModels;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_menu_view, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        changeView(holder, position);
        fillData(holder, position);
    }

    private void changeView(ViewHolder holder, int position){
        if(position == 2){
            holder.itemMenu_linearView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorWhite));
            holder.itemMenu_textView.setTextColor(ContextCompat.getColor(context, R.color.colorOrange));
        }
    }

    private void fillData(ViewHolder holder, int position) {
        holder.itemMenu_imageView.setImageDrawable(menuModels.get(position).getImage());
        holder.itemMenu_textView.setText(menuModels.get(position).getTitle());
    }

    @Override
    public int getItemCount() {
        return menuModels.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.itemMenu_textView)
        TextView itemMenu_textView;
        @BindView(R.id.itemMenu_imageView)
        ImageView itemMenu_imageView;
        @BindView(R.id.itemMenu_linearView)
        LinearLayout itemMenu_linearView;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemAction(getAdapterPosition());
        }

        private void itemAction(int position){
            switch (position){
                case 0:
                    openFragment(context, InviteFriendsFragment.class, R.id.homeContainer_frame);
                    editToolbar(context, context.getResources().getString(R.string.invite_friends),
                            CYAN_BLUE, WHITE, R.drawable.ic_back_blue);
                    break;
                case 1:
                    openFragment(context, RecentSearchesFragment.class, R.id.homeContainer_frame);
                    editToolbar(context, context.getResources().getString(R.string.recent_search),
                            CYAN_BLUE, WHITE, R.drawable.ic_back_blue);
                    break;
                case 2:
                    openFragment(context, RemoveAdsFragment.class, R.id.homeContainer_frame);
                    editToolbar(context, context.getResources().getString(R.string.remove_ads),
                            CYAN_BLUE, WHITE, R.drawable.ic_back_blue);
                    break;
                case 3:
                    openFragment(context, ChangeLanguageFragment.class, R.id.homeContainer_frame);
                    editToolbar(context, context.getResources().getString(R.string.change_language),
                            CYAN_BLUE, WHITE, R.drawable.ic_back_blue);
                    break;
                case 4:
                    openFragment(context, HelpFragment.class, R.id.homeContainer_frame);
                    editToolbar(context, context.getResources().getString(R.string.help_and_support),
                            CYAN_BLUE, WHITE, R.drawable.ic_back_blue);
                    break;
                case 5:
                    openFragment(context, HomeFragment.class, R.id.homeContainer_frame);
                    break;
            }
        }
    }


}
