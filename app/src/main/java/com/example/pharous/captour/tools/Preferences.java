package com.example.pharous.captour.tools;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pharous.captour.R;
import com.example.pharous.captour.ui.activities.HomeActivity;

import java.util.Objects;

public class Preferences {

    public static class colors{
        public static int CYAN_BLUE = R.color.colorCyanBlue;
        public static int WHITE = R.color.colorWhite;
    }

    public static class Methods{
        public static Fragment openFragment(Activity activity, Class fragmentClass, int container) {
            Fragment fragment = null;
            try {
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }
            FragmentManager fragmentManager = ((FragmentActivity)activity).getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(container, fragment).commit();
            return fragment;
        }

        public static Fragment getFragment(Activity context, int container){
            return ((AppCompatActivity) context).getSupportFragmentManager().findFragmentById(container);
        }

        public static void copyText(Activity context, String label, String text){
            ClipboardManager clipboardManager =
                    (ClipboardManager) Objects.requireNonNull(context).getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clipData = ClipData.newPlainText(label, text);
            Objects.requireNonNull(clipboardManager).setPrimaryClip(clipData);
            Toast.makeText(context, R.string.copied_successfully, Toast.LENGTH_LONG).show();
        }

        public static void editToolbar(Activity context, String title, int titleColor,
                                 int toolbarBackgroundColor, int toolbarDrawable){
            TextView textView = ((Activity)context).findViewById(R.id.toolbarTitle_textView);
            textView.setText(title);
            textView.setTextColor(ContextCompat.getColor(context, titleColor));
            Toolbar toolbar = ((Activity)context).findViewById(R.id.toolbar);
            toolbar.setBackgroundColor(ContextCompat.getColor(context, toolbarBackgroundColor));
            Objects.requireNonNull(((HomeActivity) context).getSupportActionBar()).
                    setHomeAsUpIndicator(context.getResources().getDrawable(toolbarDrawable));
        }

        public static boolean isNetworkAvailable(final Activity context) {
            NetworkInfo networkInfo = null;
            if (context != null) {
                ConnectivityManager mConnectivity =
                        (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                if (mConnectivity != null) {
                    networkInfo = mConnectivity.getActiveNetworkInfo();
                }
                return networkInfo != null && networkInfo.isConnectedOrConnecting();
            }
            return false;
        }
    }

}
