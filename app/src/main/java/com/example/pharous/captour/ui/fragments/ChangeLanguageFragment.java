package com.example.pharous.captour.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.pharous.captour.R;
import com.example.pharous.captour.base.BaseFragment;
import com.example.pharous.captour.ui.adapters.LanguageListAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 */
public class ChangeLanguageFragment extends BaseFragment {

    Unbinder unbinder;

    @BindView(R.id.language_recyclerView)
    RecyclerView language_recyclerView;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_change_language, container, false);

        unbinder = ButterKnife.bind(this, view);
        initRecyclerView();
        return view;
    }

    private void initRecyclerView(){
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        language_recyclerView.setLayoutManager(layoutManager);
        language_recyclerView.setAdapter(initAdapter());
    }

    private LanguageListAdapter initAdapter() {
        return new LanguageListAdapter(initList());
    }

    private ArrayList<String> initList(){
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("ARABIC");
        arrayList.add("ENGLISH");
        arrayList.add("FRENCH");
        arrayList.add("SPANISH");
        arrayList.add("PORTUGUESE");
        arrayList.add("JAPANESE");
        arrayList.add("CHINESE");
        arrayList.add("INDIAN");
        arrayList.add("ARABIC");
        arrayList.add("ENGLISH");
        arrayList.add("FRENCH");
        arrayList.add("SPANISH");
        arrayList.add("PORTUGUESE");
        arrayList.add("JAPANESE");
        arrayList.add("CHINESE");
        arrayList.add("INDIAN");
        return arrayList;
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
