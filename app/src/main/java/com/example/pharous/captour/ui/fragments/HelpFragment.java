package com.example.pharous.captour.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.pharous.captour.R;
import com.example.pharous.captour.base.BaseFragment;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class HelpFragment extends BaseFragment {

    Unbinder unbinder;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_help, container, false);

        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.howToUse_cardView, R.id.privacyPolicy_cardView, R.id.termsOfUse_cardView, R.id.faq_cardView})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.howToUse_cardView:
                break;
            case R.id.privacyPolicy_cardView:
                break;
            case R.id.termsOfUse_cardView:
                break;
            case R.id.faq_cardView:
                break;
        }
    }
}
