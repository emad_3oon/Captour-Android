package com.example.pharous.captour.ui.models;

import android.graphics.drawable.Drawable;

public class HomeListModel {

    private String title;
    private Drawable image;

    public HomeListModel(String title, Drawable image) {
        this.title = title;
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Drawable getImage() {
        return image;
    }

    public void setImage(Drawable image) {
        this.image = image;
    }

}
