package com.example.pharous.captour.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.example.pharous.captour.R;
import com.example.pharous.captour.base.BaseFragment;
import com.example.pharous.captour.tools.Preferences;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 */
public class InviteFriendsFragment extends BaseFragment {

    Unbinder unbinder;

    @BindView(R.id.inviteFriendsLink_textView)
    TextView inviteFriendsLink_textView;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_invite_friends, container, false);

        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @OnClick({R.id.copyLink_cardView, R.id.facebook_imageView, R.id.gmail_imageView,
            R.id.whatsapp_imageView, R.id.twitter_imageView})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.copyLink_cardView:
                Preferences.Methods.copyText(getContext(), null, inviteFriendsLink_textView.getText().toString());
                break;
            case R.id.facebook_imageView:
                Toast.makeText(getContext(), "FACEBOOK", Toast.LENGTH_LONG).show();
                break;
            case R.id.gmail_imageView:
                Toast.makeText(getContext(), "GMAIL", Toast.LENGTH_LONG).show();
                break;
            case R.id.whatsapp_imageView:
                Toast.makeText(getContext(), "WHATSAPP", Toast.LENGTH_LONG).show();
                break;
            case R.id.twitter_imageView:
                Toast.makeText(getContext(), "TWITTER", Toast.LENGTH_LONG).show();
                break;
        }
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}
