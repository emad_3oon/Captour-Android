package com.example.pharous.captour.ui.activities;

import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;
import com.example.pharous.captour.R;
import com.example.pharous.captour.base.BaseActivity;
import com.example.pharous.captour.ui.fragments.HomeFragment;
import com.example.pharous.captour.ui.fragments.MenuFragment;
import com.example.pharous.captour.ui.fragments.PlaceDetailsFragment;
import com.example.pharous.captour.ui.fragments.RecentSearchesFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.pharous.captour.tools.Preferences.Methods.getFragment;
import static com.example.pharous.captour.tools.Preferences.Methods.openFragment;

public class HomeActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbarTitle_textView)
    TextView toolbarTitle_textView;

    private ActionBar actionBar;

    private int colorCyanBlue;
    private int colorOrange;
    private int colorWhite;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ButterKnife.bind(this);
        initView();
    }

    private void initToolbar(String title, int toolbarColor, int textColor) {
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_menu));
        }
        setTitle(null);
        toolbarTitle_textView.setText(title);
        toolbarTitle_textView.setTextColor(textColor);
        toolbar.setBackgroundColor(toolbarColor);
    }

    private void initView(){
        colorCyanBlue = ContextCompat.getColor(getContext(), R.color.colorCyanBlue);
        colorOrange = ContextCompat.getColor(getContext(), R.color.colorOrange);
        colorWhite = ContextCompat.getColor(getContext(), R.color.colorWhite);
        initToolbar(getResources().getString(R.string.app_name), colorWhite, colorOrange);
        openFragment(getContext(), HomeFragment.class, R.id.homeContainer_frame);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                homeButtonAction();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void homeButtonAction(){
        if(getFragment(getContext(), R.id.homeContainer_frame) instanceof MenuFragment){
            openFragment(getContext(), HomeFragment.class, R.id.homeContainer_frame);
            initToolbar(getResources().getString(R.string.app_name), colorWhite, colorOrange);
            actionBar.setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_menu));
        }else if(getFragment(getContext(), R.id.homeContainer_frame) instanceof PlaceDetailsFragment){
            openFragment(getContext(), RecentSearchesFragment.class, R.id.homeContainer_frame);
            initToolbar(getResources().getString(R.string.recent_search), colorWhite, colorCyanBlue);
            actionBar.setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_back_blue));
        }else {
            openFragment(getContext(), MenuFragment.class, R.id.homeContainer_frame);
            initToolbar(getResources().getString(R.string.menu), colorCyanBlue, colorWhite);
            actionBar.setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_back));
        }
    }

}
