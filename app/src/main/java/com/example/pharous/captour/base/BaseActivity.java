package com.example.pharous.captour.base;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.example.pharous.captour.google.ads.GoogleAdsTools;
import com.google.android.gms.ads.MobileAds;

@SuppressLint("Registered")
public class BaseActivity extends AppCompatActivity {

    private Activity context;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = this;
    }

    public Activity getContext() {
        return context;
    }

    @Override public void onBackPressed() {
        finish();
    }
}
