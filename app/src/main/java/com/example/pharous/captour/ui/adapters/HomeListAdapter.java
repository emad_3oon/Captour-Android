package com.example.pharous.captour.ui.adapters;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.pharous.captour.ui.models.HomeListModel;
import com.example.pharous.captour.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeListAdapter extends RecyclerView.Adapter<HomeListAdapter.ViewHolder> {

    private ArrayList<HomeListModel> homeListModels;
    private ListItemClickListener itemClickListener;

    public HomeListAdapter(ArrayList<HomeListModel> homeListModels, ListItemClickListener itemClickListener) {
        this.homeListModels = homeListModels;
        this.itemClickListener = itemClickListener;
    }

    public interface ListItemClickListener {
        void onListItemClick();
    }

    @NonNull
    @Override
    public HomeListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_home_view, parent, false);
        return new HomeListAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeListAdapter.ViewHolder holder, int position) {
        fillData(holder, position);
    }

    private void fillData(HomeListAdapter.ViewHolder holder, int position) {
        holder.itemHome_imageView.setImageDrawable(homeListModels.get(position).getImage());
        holder.itemHome_textView.setText(homeListModels.get(position).getTitle());
    }

    @Override
    public int getItemCount() {
        return homeListModels.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.itemHome_textView)
        TextView itemHome_textView;
        @BindView(R.id.itemHome_imageView)
        ImageView itemHome_imageView;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onListItemClick();
        }

    }

}
